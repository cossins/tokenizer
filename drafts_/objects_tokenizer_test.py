#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test de la classe ObjectsTokenizer

"""

from objects_tokenizer import ObjectsTokenizer as Tokenizer

tok = Tokenizer(analyzer='chargram',size=8,generator=False)
tokens = tok(text)
list(tokens)
list(tok.tokens)
string = tok.undo()

tok.tokens = Tokens([Token(0,len(text),text),])
tok._split_doc(' ')


for token in tokens: print(token)
tok.tokens
token.tokens
tokens[5]
tokens[6]
len(tokens)

tokens = list(tok._split_doc(' ',text))
tokens = tok(text)
tokens = 
ntokens = [Tokenizer(analyzer='word')(t) for t in tokens]

tok = Tokenizer(analyzer='word',size=4)
len(text.split(' '))
len(tok(text))
len(tok.pipeline)

tok.transform(text)
tok(text)
tok.undo(tok(text))
tok.undo(tok(text)) == text

for tok in text.split():
    if not tok: print("empty tok")
    
tok = Tokenizer(analyzer='word',size=4)
tok.transform(text)
tok(text)
tok.pipeline
tok.undo(tok(text))

tok = Tokenizer(analyzer='sentence',generator=True)
tok(text)
tok.pipeline

tokens = [t for t in tok(text)]
tok.pipeline
tok.undo(tokens)

tok = Tokenizer(analyzer='ngram',size=4)
tok.transform(text)
tok.pipeline
tok.undo(tok(text))
tok.undo(tok(text)) == text


tok = Tokenizer(analyzer='chargram',size=14)
tok.transform(text)
tok(text)
tok.undo(tok(text))
tok.undo(tok(text)) == text

tok = Tokenizer(analyzer="word",size=6)

docs = [str(i)*8+' '+str(i)*5 for i in range(7)]
for batch in tok.pipe(docs,batch_size=5):
    print(batch)