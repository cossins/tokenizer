#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test de la classe Tokenizer

"""

from tokenizer import Tokenizer

text = "   un texte que l'on veut \t   séparer en tokens\n contenant   plusieurs phrases.\n fin du texte de test   "

tok = Tokenizer(analyzer='word')
tokens = tok(text)
ntokens = [Tokenizer(analyzer='word')(t) for t in tokens]

tok = Tokenizer(analyzer='word',size=4)
len(text.split(' '))
len(tok(text))
len(tok.pipeline)

tok.transform(text)
tok(text)
tok.undo(tok(text))
tok.undo(tok(text)) == text

for tok in text.split():
    if not tok: print("empty tok")
    
tok = Tokenizer(analyzer='word',size=4)
tok.transform(text)
tok(text)
tok.pipeline
tok.undo(tok(text))

tok = Tokenizer(analyzer='sentence',generator=True)
tok(text)
tok.pipeline

tokens = [t for t in tok(text)]
tok.pipeline
tok.undo(tokens)

tok = Tokenizer(analyzer='ngram',size=4)
tok.transform(text)
tok.pipeline
tok.undo(tok(text))
tok.undo(tok(text)) == text


tok = Tokenizer(analyzer='chargram',size=14)
tok.transform(text)
tok(text)
tok.undo(tok(text))
tok.undo(tok(text)) == text

tok = Tokenizer(analyzer="word",size=6)

docs = [str(i)*8+' '+str(i)*5 for i in range(7)]
for batch in tok.pipe(docs,batch_size=5):
    print(batch)