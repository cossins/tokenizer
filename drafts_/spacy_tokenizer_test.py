#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test du Tokenizer adapté pour spaCy

"""

from spacy_tokenizer import SpacyTokenizer
from chutal.spacychu import CHU

text = "un texte que l'on veut séparer EN tokens\n contenant plusieurs phrases\n fin du texte de test"

nlp = CHU()
tok = SpacyTokenizer(analyzer="original",size=2,vocab = nlp.vocab)
doc = tok(text)
doc[0]

tok = SpacyTokenizer(analyzer="ngram",size=2,vocab = nlp.vocab)
doc = tok(text)
doc[0]
' ; '.join(tok.text for tok in doc[2:8])

nlp = CHU()
nlp.tokenizer = SpacyTokenizer(analyzer="chargram",size=2, vocab=nlp.vocab)
doc = nlp(text)
' ; '.join(tok.text for tok in doc[2:8])

nlp = CHU()
nlp.tokenizer = SpacyTokenizer(analyzer="sentence", vocab=nlp.vocab)
for batch in nlp.pipe([text]*8,batch_size=2):
    print(' ; '.join(tok.text for tok in batch))
    print('end of batch')