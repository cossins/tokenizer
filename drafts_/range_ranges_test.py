#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test des fonctionnalités des classes Range et Ranges
"""

from itertools import combinations
from tokenizer import Range
from tokenizer import ReversibleTokenizer

r0 = Range(12,45)
r1 = Range(25)
r2 = Range(48,65)
r3 = Range(15,40)

r0.isdisjoint(r1)
r1.isdisjoint(r0)

r0 < r1
r0 <= r1
r0 > r1
r0 >= r1

r1 < r0
r1 <= r0
r1 < r0
r1 >= r0

r0 < r2
r0 <= r2
r0.issubRange(r2)
r0 > r2
r0 >= r2
r0.issuperRange(r2)

r2 < r0
r2 <= r0
r2 > r0
r2 >= r0

r2.isdisjoint(r0)

r0 <= r3
r0 >= r3
r0 < r3
r0 > r3
r3.isdisjoint(r0)

r4 = r0.union(r3)
r5 = r0.union(r1)
r6 = r1.union(r0)
r6==r5

r4==r0

R0 = r0.union(r2)

r0.intersection(r2)

# from https://stackoverflow.com/a/15273749/8844500

a = [(4,8),(7, 10), (11, 13), (56,58), (40, 56), (14, 20), (23, 39),(61,78)]
a0 = sorted(a)
b0 = [list(a0[0])]
for start,stop in a0:
    if b0[-1][1] >= start - 1:
        b0[-1][1] = max(b0[-1][1], stop)
    else:
        b0.append([start, stop])
b0 = [tuple(b) for b in b0]
