#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test de la classe Tokenizer -- ne marche pas vraiment ... 

"""
import unittest

from tokenizer import Tokenizer

text = "Un texte que l'on veut séparer en tokens\t contenant 3 phrases\n.VoiCi la deUxième phrase.\n\n\nFin du texte de test ->"

class TestTokenizer(unittest.TestCase):
    
    def _test_generic(self,analyzer,size,text,error):
        tok = Tokenizer(analyzer=analyzer,size=size)
        ntext = tok.transform(text)
        self.assertEqual(ntext,text,error+' in .transform method')
        ntext = tok(text)
        self.assertEqual(ntext,text,error+' in __call__')
    
    def test_document(self):
        self._test_generic("document",None,[text,],"Should be the same text")
    def test_original(self):
        self._test_generic('original',None,[text,],"Should be the same text")
    def test_ngram(self):
        n = 4
        toks = [tok for tok in text.split() if tok]
        tokens = (' '.join(toks[i:i+n]) for i in range(len(toks)-n+1))
        self._test_generic('ngram',n,list(tokens),
                      "Should be a list of splitted document by word of length 4")

def test_document():
    tok = Tokenizer(analyzer='document',size=None)
    ntext = tok(text)
    assert ntext == [text,]

def test_ngram(n):
    tok = Tokenizer(analyzer='ngram',size=n)
    ntext = tok(text)
    toks = [tok for tok in text.split() if tok]
    tokens = list(' '.join(toks[i:i+n]) for i in range(len(toks)-n+1))
    assert ntext == tokens, "ngram_problems"

def test_chargram(n):
    tok = Tokenizer(analyzer='chargram',size=n)
    ntext = tok(text)
    tokens = list(text[i:i+n] for i in range(len(text)-n+1))
    assert ntext == tokens, "chargram_problems"

if __name__ == '__main__':
    test_document()
    for n in [4,8,12]:
        test_ngram(n)
        test_chargram(n)
    # unittest.main()
    print("All tests ran correctly")