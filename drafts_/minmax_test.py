#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 16:26:33 2021

@author: konsfra
"""
from iamtokenizing import Token, Tokens

text = 'I am a string following a conversation'
t1 = Token(text)
t2 = Token(text,ranges=[range(5,6),])
t3 = Token(text,ranges=[range(24,25),])
t4 = Token('a')

t = t2+t3
str(t)
t.ranges
t2.ranges
t3.ranges

t = t2-t3
str(t)
t.ranges
t2.ranges
t3.ranges

t = t2-t1
str(t)
t.ranges
t2.ranges
t1.ranges

t = t1-t2
str(t)
t.ranges
t2.ranges
t1.ranges

t = t2/t3
str(t)
t.ranges
t2.ranges
t3.ranges

t = t3/t2
str(t)
t.ranges
t2.ranges
t3.ranges

t = t2*t3
str(t)
t.ranges
t2.ranges
t3.ranges

t2 = Token(text,ranges=[range(5,10),range(15,20)])
t3 = Token(text,ranges=[range(8,12),])

t = t2/t3
str(t)
t.ranges
t2.ranges
t3.ranges

t = t3/t2
str(t)
t.ranges
t2.ranges
t3.ranges

t = t2*t3
str(t)
t.ranges
t2.ranges
t3.ranges

t = t2-t1
bool(t)
t

t1-t2
