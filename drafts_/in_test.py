#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 16:26:33 2021

@author: konsfra
"""

from iamtokenizer import Token, Tokens

text = 'I am a string following a conversation'
t1 = Token(text)
t2 = Token(text,ranges=[range(5,6),])
t3 = Token(text,ranges=[range(24,25),])
t4 = Token('a')

t2 in t1
t1 in t2
t3 in t1
t1 in t3
t2 in t3
str(t2) in t3
str(t3) in t2
t4 in t2
t4 in t3
t2 in t4
t3 in t4
str(t4) in t2
str(t4) in t3

t2<t1
t1<t2
t2<=t1
t1<=t2
t3<t1
t1<t3
t4<t2
t2<t4


t = t2 + t3
t.ranges

t.subToken[1].ranges
t.subToken[2]
t.subToken[0].ranges
t.subTokens

for tok in t.subToken:
    print(tok.ranges)