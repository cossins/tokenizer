#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tokenizer class.

Main method is transform or __call__, which receives a string and return list of strings, 
each being a token.
Cut the text in word (string separated by ' '), sentence (string separeted by '\n'),
n-grams (succession of words of a given size), char-grams (uccession of characters).

"""

from itertools import islice

class Tokenizer():
    
    def _check_analyzer(self,analyzer):
        if analyzer not in ['word','sentence','ngram','chargram','document','original']:
            mess = "analyzer must be document, sentence, word, ngram or chargram"
            mess += ', received {}'.format(analyzer)
            raise ValueError(mess)
        self.analyzer = str(analyzer)
        return None
    
    def _check_size(self,size):
        if self.analyzer in ['ngram','chargram'] and not size:
            mess = "size must be provided for ngram and chargram values"
            mess += " of analyzer, received {}".format(size)
            raise ValueError(mess)
        if size:
            self.size = int(size)
        else:
            self.size = None
        return None
    
    def __init__(self,analyzer='word',size=None,generator=False):
        self._check_analyzer(analyzer)
        self._check_size(size)
        self.generator = bool(generator)
        return None
    
    def __call__(self,doc):
        return self.transform(doc)
    
    def _split_doc(self,doc):
        if self.analyzer=='word':
            tokens = (tok for tok in doc.split() if tok)
        elif self.analyzer=='ngram':
            toks = [tok for tok in doc.split() if tok]
            n = len(toks)-self.size+1
            tokens = (' '.join(toks[i:i+self.size]) for i in range(n))
        elif self.analyzer=='sentence':
            tokens = doc.split('\n')
            tokens = (tok.strip() for tok in tokens if tok)
        elif self.analyzer=='chargram':
            tokens = (doc[i:i+self.size] for i in range(len(doc)-self.size+1))
        elif self.analyzer in ['document','original']:
            tokens = (doc,)
        else:
            raise AttributeError("Didn't understand the analyzer in _split_doc")
        return tokens
    
    def transform(self,doc):
        if not self.generator:
            return list(self._split_doc(doc))
        else:
            return self._split_doc(doc)
    
    def pipe(self,docs,batch_size=1):
        iterable = iter(docs)
        batch = list(islice(iterable,batch_size))
        while batch:
            tokens = (self.transform(doc) for doc in batch)
            yield tokens
            batch = list(islice(iterable,batch_size))
            


