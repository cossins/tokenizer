#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test of Token and Tokens classes
"""

from tokenizer.tokentokens import Token, Tokens
import re

text = "   un texte que l'on veut \t   sépaRER en Tokens\n contenant  123 mg/L plusieurs phrases.\n fin du texte de test   "

shift = 45
token = Token(start=shift,string=text)
str(token)
print(token)

tokens = token.partition_token(12,23)
list(tokens)

print(len(token))
cuts = [(12,23),(12,48),(12,250),(48,58),(130,185),(185,250)]
for start,end in cuts:
    tokens = token.partition_token(start,end)
    print(list(tokens))

for start,end in cuts:
    tokens = token.partition_token(start,end,non_empty=True)
    print(list(tokens))

bool(token)
tok = token.copy()
token = Token()
bool(token)

token = tok.copy()

tokens = token.partition_token(48,54)
list(tokens)
tokens[2].upper().string
str(tokens[2].upper())
print(tokens[2].upper())

tokens[2].upper().isupper()
tokens = tokens[2].split("'")
list(tokens)
' '.join(tokens)

tokens = token.split('e')
token = Token(string=' '.join(tokens),start=token.start)
list(tokens)

tokens = token.partition('zt')
list(tokens)


list(tokens)
token[47:65]
text[47-shift:65-shift]

Z = Tokens([z.strip() for z in z])

z = a.slice(4)
list(z)

tok = z[-1]
tok[:]

toks = token.split_token([(2,5),(8,12)])
list(toks)


#%%

tokens = token.slice(5)
list(tokens)
token = tokens.undo(start=12,stop=25,step=2)
token2 = tokens.slice(start=12,stop=25,step=1)
token3 = tokens.undo(start=8,stop=25,step=5)
tokens[8:12]
