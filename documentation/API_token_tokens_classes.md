# Table of Contents

* [tokentokens](#tokentokens)
  * [Token](#tokentokens.Token)
    * [\_\_new\_\_](#tokentokens.Token.__new__)
    * [\_\_getattribute\_\_](#tokentokens.Token.__getattribute__)
    * [\_\_init\_\_](#tokentokens.Token.__init__)
    * [\_\_len\_\_](#tokentokens.Token.__len__)
    * [\_\_repr\_\_](#tokentokens.Token.__repr__)
    * [\_\_bool\_\_](#tokentokens.Token.__bool__)
    * [\_\_getitem\_\_](#tokentokens.Token.__getitem__)
    * [copy](#tokentokens.Token.copy)
    * [partition\_token](#tokentokens.Token.partition_token)
    * [slice](#tokentokens.Token.slice)
    * [split\_token](#tokentokens.Token.split_token)
  * [Tokens](#tokentokens.Tokens)
    * [\_\_init\_\_](#tokentokens.Tokens.__init__)
    * [\_\_repr\_\_](#tokentokens.Tokens.__repr__)
    * [\_\_len\_\_](#tokentokens.Tokens.__len__)
    * [\_\_add\_\_](#tokentokens.Tokens.__add__)
    * [\_\_str\_\_](#tokentokens.Tokens.__str__)
    * [\_\_getitem\_\_](#tokentokens.Tokens.__getitem__)
    * [append](#tokentokens.Tokens.append)
    * [undo](#tokentokens.Tokens.undo)
    * [slice](#tokentokens.Tokens.slice)

<a name="tokentokens"></a>
# tokentokens

`Token` and `Tokens` classes

`Token` sub-classes the `string` class in Python, thus enabling basic usages of
string (as e.g. split, isupper, lower, ... see 
https://docs.python.org/3.8/library/string.html)
in addition to enabling additional attribute in the flow, for later 
compatibility with other packages. Its main original methods are
 - `partition_token(start,stop)` : which partitions the initial `Token` in three
 new `Token` instance, collected in a unique `Tokens` instance (see below)
 - `split_token([(start,end),(start2,end2), ...])` : which splits the `Token`
 in several instances grouped in a single `Tokens` object
 - `slice(step)` : which slices the initial string in overlapping sub-strings,
 all grouped in a single `Tokens` instance

`Tokens` collects the different `Token` instances in order to let the initial 
`Token` instance still glued somehow. It also allows to come back to `Token`
instances using its original methods : 
     - `undo(start,stop,step)` : which generate a unique `Token` from the `Tokens`
     elements `Tokens[start:stop:step]`
     - `slice(start,stop,step)` : which slices the list of `Token` instance and 
     glue them in some overlapping strings. This is still a `Tokens` instance

<a name="tokentokens.Token"></a>
## Token Objects

```python
class Token(str)
```

Subclass of the Python string class.
It allows manipulating a string as a usual Python object, excepts it 
returns Token instances. Especially for Token instances are the 
methods : 
 - `split(s)`, which splits the string everytime the string 's' appears
 - `isupper()` (for instance), which tells whether the string is 
 uppercase or not
 -` lower()` or `upper()`, to make the `Token` lower-/upper-case.
see more string methods on the Python standard library documentation.
https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str

In addition, having class associated to a Token allows to add custom
   	attribute and method at any moment during its use.

<a name="tokentokens.Token.__new__"></a>
#### \_\_new\_\_

```python
 | __new__(cls, start=0, string=str())
```

Call the Python `string` instance.

Inspired from https://stackoverflow.com/a/51762208

<a name="tokentokens.Token.__getattribute__"></a>
#### \_\_getattribute\_\_

```python
 | __getattribute__(name)
```

Apply Python `string` methods to the `Token.string` entity if this
method exists in `__dir__(str)`. 
Otherwise apply the corresponding method of the `Token` class.

Inspired from https://stackoverflow.com/a/33272874

<a name="tokentokens.Token.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(start=0, string=str())
```

`Token` object is basically a `string` with a `start` (integer) position.
Its basic usage is : 
 - `string` extracted from position `start`
and the only two attributes are
 - `Token.string`	-> a string
 - `Token.start`	-> an integer keeping track of a possible 
 larger `Token` (or a larger `string`).

<a name="tokentokens.Token.__len__"></a>
#### \_\_len\_\_

```python
 | __len__()
```

Return the length of the `Token.string` attribute

<a name="tokentokens.Token.__repr__"></a>
#### \_\_repr\_\_

```python
 | __repr__()
```

Return the two main arguments (namely `start` and `string`) of a 
`Token` instance in a readable way. For debugging mainly.

<a name="tokentokens.Token.__bool__"></a>
#### \_\_bool\_\_

```python
 | __bool__()
```

Returns `True` if the `Token.string` is non-empty, otherwise 
return `False`

<a name="tokentokens.Token.__getitem__"></a>
#### \_\_getitem\_\_

```python
 | __getitem__(n)
```

Allows slice and integer catch of the elements of `Token.string`. 
Return empty string in case there is no element associated to the 
string, e.g. for a position outside the `Token` instance.
Return empty string if the slice is outside the `Token` instance.
Return partial string if the slice starts or ends outside the 
range of the `Token.string` attribute.

<a name="tokentokens.Token.copy"></a>
#### copy

```python
 | copy()
```

Returns a copy of the actual `Token` instance.

<a name="tokentokens.Token.partition_token"></a>
#### partition\_token

```python
 | partition_token(start, end, non_empty=False)
```

Split the `Token.string` in three `Token` objects : 
    - `string[:start]`
    - `string[start:stop]`
    - `string[stop:]`
and put all non-empty `Token` objects in a `Tokens` instance.

It acts a bit like the `str.partition(s)` method of the Python
`string` object, but `partition_token` takes `start è and `end` 
argument instead of a string. So in case one wants to split a string in three 
sub-strings using a string 's', use `Token.partition(s)` instead, 
inherited from `str.partition(s)`.
NB : `Token.partition(s)` has no `non_empty` option.

Parameters
----------
`start` : int
    Starting position of the splitting sequence.
`end` : int
    Ending position of the splitting sequence.
`non_empty` : bool. Default is `False`
    If `True`, returns a `Tokens` instance with only non-empty 
    `Token` objects.
    see __bool__() method for non

Returns
-------
tokens : `Tokens` object
    The `Tokens` object containing the different non-empty `Token` objects.

<a name="tokentokens.Token.slice"></a>
#### slice

```python
 | slice(size)
```

Cut the `Token.string` in overlapping sequences of strings of size `size`,
put all these sequences in separated `Token` objects, and finally 
put all theses objects in a `Tokens` instance.

Parameters
----------
`size` : int
    The size of the string in each subsequent Token objects.

Returns
-------
`tokens` : `Tokens` object
    The `Tokens` object containing the different `Token` sliced objects.

<a name="tokentokens.Token.split_token"></a>
#### split\_token

```python
 | split_token(cuts)
```

Split a text as many times as there are entities in the cuts list.
Return a `Tokens` instance.

This is a bit like `str.split(s)` method from Python `string`
object, except one has to feed `Token.split_token` with a full list
of `(start,end)` tuples instead of the string 's' in `str.split(s)`
If the `(start,end)` tuples in cuts are given by a regex re.finditer
search, the two methods give the same thing. So in case one wants
to split a string in several `Token` instances according to a 
string 's' splitting procedure, use `Token.split(s)` instead of 
`Token.split_token([(start,end), ...])`.

Parameters
----------
`cuts` : a list of `(start,end,)` tuples. start/end are integer
    Basic usage is to take these cuts from [`re.finditer`](https://docs.python.org/3/library/re.html#re.finditer).

Return
------
`tokens` : A `Tokens` object
    A `Tokens` instance containing all the `Token` instances of the
    individual tokens.

<a name="tokentokens.Tokens"></a>
## Tokens Objects

```python
class Tokens()
```

A tool class for later uses in `Tokenizer` class (not documented in this module).
This is mainly a list of `Token` objects, with additional methods to 
implement string manipulations, and go back to individual `Token` instances.

<a name="tokentokens.Tokens.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(tokens=list())
```

`Tokens` instance is just a list of `Token` instances, called
 - `Tokens.tokens`	-> a list attribute.
The only verification is that the `Token.copy()` methods works.

<a name="tokentokens.Tokens.__repr__"></a>
#### \_\_repr\_\_

```python
 | __repr__()
```

Representation of the `Tokens` class, printing the number of `Token`
instances inside the `Tokens` one, and the concatenated string from
all `Tokens` instances.

<a name="tokentokens.Tokens.__len__"></a>
#### \_\_len\_\_

```python
 | __len__()
```

Return the number of `Token` instances in the `Tokens` object.

<a name="tokentokens.Tokens.__add__"></a>
#### \_\_add\_\_

```python
 | __add__(tokens)
```

Add two `Tokens` instances in the same way `list` can be concatenated :  
by concatenation of their tokens list of `Token` instances.

<a name="tokentokens.Tokens.__str__"></a>
#### \_\_str\_\_

```python
 | __str__()
```

Return the concatenated string of all the `Token` instances in the
`Tokens.tokens` attribute.

<a name="tokentokens.Tokens.__getitem__"></a>
#### \_\_getitem\_\_

```python
 | __getitem__(n)
```

Return either a `Tokens` new instance in case a slice is given,
or the `Token` instance correspondig to the position n in case
an integer is catched as argument.

<a name="tokentokens.Tokens.append"></a>
#### append

```python
 | append(token)
```

Append a `Token` instance to the actual `Tokens` instance.

<a name="tokentokens.Tokens.undo"></a>
#### undo

```python
 | undo(start=0, stop=None, step=1)
```

Glue the different `Token` objects present in the `Tokens` instance at 
position `[start:stop:step]`.
Return a `Token` instance.
    - `step = 1` : undo the `Token.split_token` or `Token.partition_token`
    methods
    - `step > 1` : undo the `Token.slice(step)` method

Parameters
----------
`start` : int, optional. The default is 0.
    Starting `Token` from which the gluing starts. 
`stop` : int, optional. The default is `None`, in which case the stop is
at the end of the string.
    Ending `Token` at which the gluing stops.
`step` : int, optional. The default is 1.
    The step in the `Tokens.tokens` list.
    If `step = 1`, undo the `Token.split_token` method
    If `step = Token.slice(step)`, undo the `Token.slice` method

Returns
-------
tokens : A `Token` instance
    The container containing the glued string.

<a name="tokentokens.Tokens.slice"></a>
#### slice

```python
 | slice(start=0, stop=None, size=1, step=1)
```

Glue the different `Token` objects present in the `Tokens.tokens` list
and returns a list of `Token objects` with overlapping strings among
the different `Token` objects, all together grouped in a `Tokens` instance.

Parameters
----------
`start` : int, optional. The default is 0.
    Starting `Token` from which the gluing starts. 
`stop` : int, optional. The default is `None`, in which case the stop is
at the end of the string.
    Ending `Token` at which the gluing stops.
`step` : int, optional. The default is 1.
    The step in the Tokens.tokens list.
    If `step = 1`, give back the initial `Tokens` object.
    If `step = n > 1`, give some n-grams `Token` by `Token`

Returns
-------
tokens : `Tokens` object
    `Tokens` objects containing the list of n-grams `Token` objects.

