# Classe ReversibleTokenizer

La classe `ReversibleTokenizer` du module `reversible_tokenizer` permet de découper une chaîne de caractère en [token](https://fr.wikipedia.org/wiki/Analyse_lexicale).

Elle permet d'extraire en particulier : 
 - les documents initiaux, sans modification (par le paramètre `analyzer='document'` ou `analyzer='original'`. Cela ne sert à peu près à rien, et ne survit que pour des raisons de rétro-compatibilité
 - les _n-grams_, qui sont des ensemble de mots contigüs (paramètres `analyzer='ngram', size=_x_`, avec `_x_` un entier qui définit le nombre de mots contigüs pris par chaque token)
 - les _char-grams_, qui sont des ensembles de caractères contigüs (paramètres `analyzer='chargram', size=_x_`)

Ces paramètres sont à rentrer dans l'appel de l'instance de classe.

Le `ReversibleTokenizer` n'apprend rien, donc la méthode `.fit` ne sert à rien. Elle n'est pas implémentée. La méthode `.transform` génère la chaîne de caractère découpée, sous forme d'une liste de tokens (i.e. une liste de strings).

On peut également tout simplement appeler `tokenizer(texte)` pour générer le texte découpé.

On donne quelques exemples de ces appels dans la suite de ce document.


```python
from reversible_tokenizer import ReversibleTokenizer as Tokenizer

text = "Un texte que l'on veut séparer en tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin du texte . . . de test ->"

print(text)
```

    Un texte que l'on veut séparer en tokens	 contenant 3 phrases arc-en-ciel.
    VoiCi ;:/?! la deUxième phrase.
    
    
    Fin du texte . . . de test ->


## `original` ou `document`

L'attribut `analyzer='original'` ou `attribut='document'` ne fait rien au texte original. Il n'est pas nécessaire dans ce cas de définir l'attribut `size`.


```python
tokenizer = Tokenizer(analyzer='document')
tokenizer(text)
```




    ["Un texte que l'on veut séparer en tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin du texte . . . de test ->"]




```python
tokenizer = Tokenizer(analyzer='original')
tokenizer.transform(text)
```




    ["Un texte que l'on veut séparer en tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin du texte . . . de test ->"]



Appeler `tokenizer(text)` ou passer par la méthode `tokenizer.transform(text)` renvoient la même chose. Ceci pour des raisons de compatibilité avec `spaCy` et `sklearn`.


```python
tokenizer.transform(text) == tokenizer(text)
```




    True



## `sentence`

L'attribut `analyzer='sentence'` permet de découper sur les retour-chariot `\n`. En cas de paragraphe, il découpera également les répétitions multiples de retour-chariot.


```python
tokenizer = Tokenizer(analyzer='sentence')
tokenizer(text)
```




    ["Un texte que l'on veut séparer en tokens\t contenant 3 phrases arc-en-ciel.",
     'VoiCi ;:/?! la deUxième phrase.',
     'Fin du texte . . . de test ->']




```python
new_text = text + "\n\n\nDernier paragraphe rajouté à l'instant"
print(new_text)
tokenizer(new_text)
```

    Un texte que l'on veut séparer en tokens	 contenant 3 phrases arc-en-ciel.
    VoiCi ;:/?! la deUxième phrase.
    
    
    Fin du texte . . . de test ->
    
    
    Dernier paragraphe rajouté à l'instant





    ["Un texte que l'on veut séparer en tokens\t contenant 3 phrases arc-en-ciel.",
     'VoiCi ;:/?! la deUxième phrase.',
     'Fin du texte . . . de test ->',
     "Dernier paragraphe rajouté à l'instant"]



## `word`

L'argument `analyzer='word'` découpe le texte sur les espaces. Il garde les tirets, les ponctuations, ... intactes. `Tokenizer` ne fait aucun nettoyage du texte. L'argument `size` n'est pas pris en compte pour ce tokenizer.


```python
tokenizer = Tokenizer(analyzer='word')
tokenizer(text)
```




    ['Un',
     'texte',
     'que',
     "l'on",
     'veut',
     'séparer',
     'en',
     'tokens\t',
     'contenant',
     '3',
     'phrases',
     'arc-en-ciel.\nVoiCi',
     ';:/?!',
     'la',
     'deUxième',
     'phrase.\n\n\nFin',
     'du',
     'texte',
     '.',
     '.',
     '.',
     'de',
     'test',
     '->']



## `ngram` de taille donnée

Pour découper sur des ensembles de mots (toujours découpé sur des espaces), il faut appeler `analyzer='ngram` et dans ce cas le paramètre `size` doit être indiqué explicitement, sous peine d'un renvoi d'erreur.


```python
tokenizer = Tokenizer(analyzer='ngram', size=3)
tokenizer(text)
```




    ['Un texte que',
     "texte que l'on",
     "que l'on veut",
     "l'on veut séparer",
     'veut séparer en',
     'séparer en tokens\t',
     'en tokens\t contenant',
     'tokens\t contenant 3',
     'contenant 3 phrases',
     '3 phrases arc-en-ciel.\nVoiCi',
     'phrases arc-en-ciel.\nVoiCi ;:/?!',
     'arc-en-ciel.\nVoiCi ;:/?! la',
     ';:/?! la deUxième',
     'la deUxième phrase.\n\n\nFin',
     'deUxième phrase.\n\n\nFin du',
     'phrase.\n\n\nFin du texte',
     'du texte .',
     'texte . .',
     '. . .',
     '. . de',
     '. de test',
     'de test ->']




```python
tokenizer = Tokenizer(analyzer='ngram', size=8)
tokenizer(text)
```




    ["Un texte que l'on veut séparer en tokens\t",
     "texte que l'on veut séparer en tokens\t contenant",
     "que l'on veut séparer en tokens\t contenant 3",
     "l'on veut séparer en tokens\t contenant 3 phrases",
     'veut séparer en tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi',
     'séparer en tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?!',
     'en tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?! la',
     'tokens\t contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième',
     'contenant 3 phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin',
     '3 phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin du',
     'phrases arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin du texte',
     'arc-en-ciel.\nVoiCi ;:/?! la deUxième phrase.\n\n\nFin du texte .',
     ';:/?! la deUxième phrase.\n\n\nFin du texte . .',
     'la deUxième phrase.\n\n\nFin du texte . . .',
     'deUxième phrase.\n\n\nFin du texte . . . de',
     'phrase.\n\n\nFin du texte . . . de test',
     'du texte . . . de test ->']



## `chargram` de taille donnée

On peut découper le texte en suite de chaîne de caractères de taille donnée, à l'aide de l'argument `analyzer='chargram'` et son paramètre `size` définissant la taille.


```python
tokenizer = Tokenizer(analyzer='chargram', size=4)
tokenizer(text)
```




    ['Un t',
     'n te',
     ' tex',
     'text',
     'exte',
     'xte ',
     'te q',
     'e qu',
     ' que',
     'que ',
     'ue l',
     "e l'",
     " l'o",
     "l'on",
     "'on ",
     'on v',
     'n ve',
     ' veu',
     'veut',
     'eut ',
     'ut s',
     't sé',
     ' sép',
     'sépa',
     'épar',
     'pare',
     'arer',
     'rer ',
     'er e',
     'r en',
     ' en ',
     'en t',
     'n to',
     ' tok',
     'toke',
     'oken',
     'kens',
     'ens\t',
     'ns\t ',
     's\t c',
     '\t co',
     ' con',
     'cont',
     'onte',
     'nten',
     'tena',
     'enan',
     'nant',
     'ant ',
     'nt 3',
     't 3 ',
     ' 3 p',
     '3 ph',
     ' phr',
     'phra',
     'hras',
     'rase',
     'ases',
     'ses ',
     'es a',
     's ar',
     ' arc',
     'arc-',
     'rc-e',
     'c-en',
     '-en-',
     'en-c',
     'n-ci',
     '-cie',
     'ciel',
     'iel.',
     'el.\n',
     'l.\nV',
     '.\nVo',
     '\nVoi',
     'VoiC',
     'oiCi',
     'iCi ',
     'Ci ;',
     'i ;:',
     ' ;:/',
     ';:/?',
     ':/?!',
     '/?! ',
     '?! l',
     '! la',
     ' la ',
     'la d',
     'a de',
     ' deU',
     'deUx',
     'eUxi',
     'Uxiè',
     'xièm',
     'ième',
     'ème ',
     'me p',
     'e ph',
     ' phr',
     'phra',
     'hras',
     'rase',
     'ase.',
     'se.\n',
     'e.\n\n',
     '.\n\n\n',
     '\n\n\nF',
     '\n\nFi',
     '\nFin',
     'Fin ',
     'in d',
     'n du',
     ' du ',
     'du t',
     'u te',
     ' tex',
     'text',
     'exte',
     'xte ',
     'te .',
     'e . ',
     ' . .',
     '. . ',
     ' . .',
     '. . ',
     ' . d',
     '. de',
     ' de ',
     'de t',
     'e te',
     ' tes',
     'test',
     'est ',
     'st -',
     't ->']



## Créer des générateurs plutôt que des séquences : `generator=True`.

Si l'on préfère créer des générateurs plutôt que des séquences, il suffit d'appeler l'argument `generator=True` pour l'instance de classe.


```python
tokenizer = Tokenizer(analyzer='ngram', size=4, generator=True)
tokenizer(text)
```




    <generator object ReversibleTokenizer._generate at 0x7fc9d46d2650>




```python
for tok in tokenizer(text):
    print(tok)
```

    Un texte que l'on
    texte que l'on veut
    que l'on veut séparer
    l'on veut séparer en
    veut séparer en tokens	
    séparer en tokens	 contenant
    en tokens	 contenant 3
    tokens	 contenant 3 phrases
    contenant 3 phrases arc-en-ciel.
    VoiCi
    3 phrases arc-en-ciel.
    VoiCi ;:/?!
    phrases arc-en-ciel.
    VoiCi ;:/?! la
    arc-en-ciel.
    VoiCi ;:/?! la deUxième
    ;:/?! la deUxième phrase.
    
    
    Fin
    la deUxième phrase.
    
    
    Fin du
    deUxième phrase.
    
    
    Fin du texte
    phrase.
    
    
    Fin du texte .
    du texte . .
    texte . . .
    . . . de
    . . de test
    . de test ->


Attention, cela ne fonctionne pas pour les attributs `analyzer='document'` ou `analyzer='original'`.


```python
tokenizer = Tokenizer(analyzer='document', size=4, generator=True)
tokenizer(text)
```




    <generator object ReversibleTokenizer._generate at 0x7fc9d46d2ad0>



## `Tokenizer.pipe`

Pour des raisons de compatibilité avec `spaCy`, on a implémenté la méthode `.pipe` qui prend en entrée une séquence de chaînes de caractères, et renvoie un générateur de tokenizer sous le forme de batch. 

Dans l'exemple ci-dessous, on place en entrée une liste de cinq textes de la forme `Texte n°1`, `Texte n°2`, ...


```python
texts = ['Texte n° '+str(i) for i in range(5)]
tokenizer = Tokenizer(analyzer='chargram', size=4, generator=False)
for batch in tokenizer.pipe(texts, batch_size=3):
    for tok in batch:
        print(tok)
    print("__ new batch __")
```

    ['Text', 'exte', 'xte ', 'te n', 'e n°', ' n° ', 'n° 0']
    ['Text', 'exte', 'xte ', 'te n', 'e n°', ' n° ', 'n° 1']
    ['Text', 'exte', 'xte ', 'te n', 'e n°', ' n° ', 'n° 2']
    __ new batch __
    ['Text', 'exte', 'xte ', 'te n', 'e n°', ' n° ', 'n° 3']
    ['Text', 'exte', 'xte ', 'te n', 'e n°', ' n° ', 'n° 4']
    __ new batch __


`Tokenizer.pipe` est un générateur. Chaque élément de `batch` est lui même un générateur.


```python
list(tokenizer.pipe(texts,batch_size=3))
```




    [<generator object ReversibleTokenizer.pipe.<locals>.<genexpr> at 0x7fc9d466f150>,
     <generator object ReversibleTokenizer.pipe.<locals>.<genexpr> at 0x7fc9d466f2d0>]


