# Table of Contents

* [objects\_tokenizer](#objects_tokenizer)
  * [ObjectsTokenizer](#objects_tokenizer.ObjectsTokenizer)

<a name="objects_tokenizer"></a>
# objects\_tokenizer

ObjectsTokenizer class. Constructed on top of Token and Tokens classes. Token subclasses the Python string, and Tokens catch a collection of Token instances.

Main method is transform or __call__, which receives a string and return list of strings, 
each being a token.
Cut the text in word (string separated by ' '), sentence (string separated by '\n'),
n-grams (succession of words of a given size), char-grams (succession of characters
of a given size) and give back a list of objects having all methods and arguments
of the Python string class, with the extra possibility to add special attributes
to these objects.

<a name="objects_tokenizer.ObjectsTokenizer"></a>
## ObjectsTokenizer Objects

```python
class ObjectsTokenizer()
```

Tokenizer class making Token objects as element.
This allow in particular to add attribute to the different tokens.

