# Documentation of the package `Tokenizer`

There are several different tokenizers in this package. Most of them have been constructed from the `Token` and `Tokens` class.

## `Token` and `Tokens` classes

The `Token` and `Tokens` classes are kind of containers usefull to construct elaborated tokenizers. They work together since a `Token` object (kind of a class constructed on top of Python `string`) transforms to a `Tokens` one once splitted (`Tokens` class can be seen as a collection of `Token` instances).

The [`Token` and `Tokens` classes API](tokentokens_API.md) presents all methods available for these two classes.

The `Token` and `Tokens` classes are explained in a series of documents, either in Markdown or Jupyter-Notebook format: 

 - [TokenTokens_1_Basics](TokenTokens_1_Basics.md) : Understanding the difference between `Token` (basically a string with associated non-overlapping ranges for extracting sub-strings) and `Tokens` (a list of `Token`) classes
 - [TokenTokens_2_RangesAndSpans](TokenTokens_2_RangesAndSpans.md) : Understanding the underlying concept of ranges of non-overlapping sub-strings in a `Token` instance, and the relation between the absolute positions (position inside the mother string) and the relative positions (position inside the token)
 - [TokenTokens_3_BasicExample](TokenTokens_3_BasicExample.md) : Construction of a basic example of tokenizer
 - [TokenTokens_4_AttributesDefinition](TokenTokens_4_AttributesDefinition.md) : COnstruction of the attributes associated to a `Token` instance, and understanding of the transmission to the `Tokens` instances.

## Advanced Tokenizers

There are several more elaborated tokenizer constructed from the `Token` and `Tokens` classes. Those are not documented at the moment, or their documentation refers to older versions of the `iamtokenizer`, which are not fully retro-compatible with `Token` and `Tokens` classes.
