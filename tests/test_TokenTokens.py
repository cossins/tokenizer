#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test of the Facilities functions working over the Token and Tokens classes

Run as 

python3 -m unittest -v tests/test_TokenTokens.py
"""

import unittest as ut

from iamtokenizing import Token, Tokens

# # for construction
# class Empty(): pass
# self = Empty()

class test_Token2Tokens(ut.TestCase):
    
    def setUp(self):
        root = '123456789'
        self.text = ''.join([str(i)+root for i in range(10)])
        self.ranges = [range(10,20),range(30,40),range(50,69)]
        self.token = Token(string=self.text,
                           ranges=self.ranges,
                           subtoksep='_')
        return None
    
    def test_partition(self,):
        tokens = self.token.partition(12,15)
        self.assertTrue(str(tokens[0])==str(self.token)[:12])
        self.assertTrue(str(tokens[1])==str(self.token)[12:15])
        self.assertTrue(str(tokens[2])==str(self.token)[15:])
        self.assertTrue(tokens[0].string==self.text)
        self.assertTrue(tokens[1].string==self.text)
        self.assertTrue(tokens[2].string==self.text)
        self.assertTrue(tokens[0].ranges == [range(10,20),range(30,31),])
        self.assertTrue(tokens[1].ranges == [range(31,34),])
        self.assertTrue(tokens[2].ranges == [range(34,40),range(50,69),])
        tokens = self.token.partition(20,30)
        self.assertTrue(str(tokens[0])==str(self.token)[:20])
        self.assertTrue(str(tokens[1])==str(self.token)[20:30])
        self.assertTrue(str(tokens[2])==str(self.token)[30:])
        self.assertTrue(tokens[0].string==self.text)
        self.assertTrue(tokens[1].string==self.text)
        self.assertTrue(tokens[2].string==self.text)
        self.assertTrue(tokens[0].ranges == [range(10,20),range(30,39),])
        self.assertTrue(tokens[1].ranges == [range(39,40),range(50,58)])
        self.assertTrue(tokens[2].ranges == [range(58,69),])
        tokens = self.token.partition(21,31)
        self.assertTrue(str(tokens[0])==str(self.token)[:21])
        self.assertTrue(str(tokens[1])==str(self.token)[21:31])
        self.assertTrue(str(tokens[2])==str(self.token)[31:])
        self.assertTrue(tokens[0].string==self.text)
        self.assertTrue(tokens[1].string==self.text)
        self.assertTrue(tokens[2].string==self.text)
        self.assertTrue(tokens[0].ranges == [range(10,20),range(30,40),])
        self.assertTrue(tokens[1].ranges == [range(40,40),range(50,59),])
        self.assertTrue(tokens[2].ranges == [range(59,69),])
        tokens = self.token.partition(22,31)
        self.assertTrue(str(tokens[0])==str(self.token)[:22])
        self.assertTrue(str(tokens[1])==str(self.token)[22:31])
        self.assertTrue(str(tokens[2])==str(self.token)[31:])
        self.assertTrue(tokens[0].string==self.text)
        self.assertTrue(tokens[1].string==self.text)
        self.assertTrue(tokens[2].string==self.text)
        self.assertTrue(tokens[0].ranges == [range(10,20),range(30,40),range(50,50)])
        self.assertTrue(tokens[1].ranges == [range(50,59),])
        self.assertTrue(tokens[2].ranges == [range(59,69),])
        tokens = self.token.partition(0,len(self.token))
        self.assertTrue(str(tokens[0])==str(self.token)[0:0])
        self.assertTrue(str(tokens[1])==str(self.token)[0:len(self.token)])
        self.assertTrue(str(tokens[2])==str(self.token)[len(self.token):len(self.token)])
        self.assertTrue(tokens[0].string==self.text)
        self.assertTrue(tokens[1].string==self.text)
        self.assertTrue(tokens[2].string==self.text)
        self.assertTrue(tokens[0].ranges == [range(10,10),])
        self.assertTrue(tokens[1].ranges == self.token.ranges)
        self.assertTrue(tokens[2].ranges == [range(69,69),])
        tokens = self.token.partition(0,len(self.token),remove_empty=True)
        self.assertTrue(len(tokens)==1)
        self.assertTrue(tokens[0].ranges == self.token.ranges)
        self.assertTrue(str(tokens[0])==str(self.token))
        with self.assertRaises(IndexError):
            tokens[1].ranges
            tokens[2].ranges
        tokens = self.token.partition(-5,-1)
        self.assertTrue(tokens[1].ranges == self.token.ranges)
        self.assertTrue(str(tokens[0]) == str())
        self.assertTrue(str(tokens[1]) == str(self.token))
        self.assertTrue(str(tokens[2]) == str())
        tokens = self.token.partition(80,100)
        self.assertTrue(tokens[0].ranges == self.token.ranges)
        self.assertTrue(str(tokens[0]) == str(self.token))
        self.assertTrue(str(tokens[1]) == str())
        self.assertTrue(str(tokens[2]) == str())
        tokens = self.token.partition(-5,-1,remove_empty=True)
        self.assertTrue(tokens[0].ranges == self.token.ranges)
        self.assertTrue(str(tokens[0]) == str(self.token))
        with self.assertRaises(IndexError):
            tokens[1].ranges
            tokens[2].ranges
        tokens = self.token.partition(80,100,remove_empty=True)
        self.assertTrue(tokens[0].ranges == self.token.ranges)
        self.assertTrue(str(tokens[0]) == str(self.token))
        with self.assertRaises(IndexError):
            tokens[1].ranges
            tokens[2].ranges
        return None
    
    def test_sliceToken(self,):
        tokens = self.token.slice(0,20)
        s = ''.join(str(tok) for tok in tokens)
        self.assertTrue(str(self.token)[:20]==s)
        
        tokens = self.token.slice(5,15,2)
        s1 = ''.join(str(tok) for tok in tokens)
        s2 = ''.join(str(self.token)[i:i+2] for i in range(5,15-2+1))
        self.assertTrue(s1==s2)
        
        tokens = self.token.slice(-5,-1,3)
        s1 = ''.join(str(tok) for tok in tokens)
        s2 = ''.join(str(self.token)[i:i+3] for i in range(len(self.token)-3+1))
        self.assertTrue(s1==s2)
        
        tokens = self.token.slice(size=3)
        s1 = ''.join(str(tok) for tok in tokens)
        self.assertTrue(s1==s2)
        
        tokens = self.token.slice(len(self.token)+10,len(self.token)+25,3)
        s1 = ''.join(str(tok) for tok in tokens)
        self.assertTrue(s1==str())
        
        tokens = self.token.slice(15,len(self.token)+25,3)
        s1 = ''.join(str(tok) for tok in tokens)
        s2 = ''.join(str(self.token)[i:i+3] for i in range(15,len(self.token)-3+1))
        self.assertTrue(s1==s2)
        
        tokens = self.token.slice(15,len(self.token)+25,3,2)
        s1 = ''.join(str(tok) for tok in tokens)
        s2 = ''.join(str(self.token)[i:i+3] for i in range(15,len(self.token)-3+1,2))
        self.assertTrue(s1==s2)
        return None
    
    def test_split(self,):
        cuts = [range(5,15),range(25,35),]
        tokens = self.token.split(cuts)
        self.assertTrue(len(tokens)==5)
        self.assertTrue(str(tokens[0]) == str(self.token)[:5])
        self.assertTrue(str(tokens[1]) == str(self.token)[5:15])
        self.assertTrue(str(tokens[2]) == str(self.token)[15:25])
        self.assertTrue(str(tokens[3]) == str(self.token)[25:35])
        self.assertTrue(str(tokens[4]) == str(self.token)[35:])
        
        toks2 = self.token.split([(5,15),(25,35),])
        self.assertTrue(len(toks2) == len(tokens))
        bools = [tok1==tok2 for tok1,tok2 in zip(tokens,toks2)]
        self.assertTrue(all(bools))
        return None
    
    def test_join(self,):
        cuts = [range(5,15),range(25,35),]
        tokens = self.token.split(cuts)
        token = tokens.join()
        self.assertTrue(str(token)==str(self.token))
        self.assertTrue(token.ranges==self.token.ranges)
        tok0 = tokens[0]
        tok2 = tokens[2]
        self.assertTrue(str(tok0)==str(self.token)[0:5])
        self.assertTrue(str(tok2)==str(self.token)[15:25])
        token = tokens.join(0,3,2)
        self.assertTrue(token.ranges==[range(10, 15), range(34, 40), range(50, 53)])
        self.assertTrue(str(token)==str(self.token)[0:5]+token.subtoksep+str(self.token)[15:25])
        return None
    
    def test_sliceTokens(self,):
        cuts = [range(5,15),range(25,35),range(40,41)]
        tokens = self.token.split(cuts)
        stokens = tokens.slice(0,6,2)
        self.assertTrue(str(stokens[0])==str(tokens[0])+str(tokens[1]))
        self.assertTrue(str(stokens[1])==str(tokens[1])+str(tokens[2]))
        self.assertTrue(str(stokens[2])==str(tokens[2])+str(tokens[3]))
        self.assertTrue(str(stokens[3])==str(tokens[3])+str(tokens[4]))
        self.assertTrue(str(stokens[4])==str(tokens[4])+str(tokens[5]))
        self.assertTrue(len(stokens)==5)
        stokens = tokens.slice(0,6,3)
        self.assertTrue(str(stokens[0])==str(tokens[0])+str(tokens[1])+str(tokens[2]))
        self.assertTrue(str(stokens[1])==str(tokens[1])+str(tokens[2])+str(tokens[3]))
        self.assertTrue(str(stokens[2])==str(tokens[2])+str(tokens[3])+str(tokens[4]))
        self.assertTrue(str(stokens[3])==str(tokens[3])+str(tokens[4])+str(tokens[5]))
        self.assertTrue(len(stokens)==4)
        return None
        

if __name__ == '__main__':
    ut.main()
