#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test of the Tokens class

Run as 

python3 -m unittest -v tests/test_TokensBasics.py
"""

import unittest as ut

from iamtokenizing import Token, Tokens

# # for construction
# class Empty(): pass
# self = Empty()

class test_TokensBasics(ut.TestCase):
    
    def setUp(self):
        root = '123456789'
        self.text = ''.join([str(i)+root for i in range(10)])
        self.tok1 = Token(string=self.text,
                          ranges=[range(10,20),range(30,50)],
                          subtoksep='_')
        self.tok2 = Token(string=self.text,
                          ranges=[range(20,30),],
                          subtoksep='_')
        self.tok3 = Token(string=self.text,
                          ranges=[range(20),range(50,80),range(85,100)],
                          subtoksep='_')      
        self.tokens = Tokens([self.tok1,self.tok2,self.tok3])
        return None
    
    def test_basicsListBehaviors(self,):
        """Verify __add__, append and extend in addition to __str__ and __len__ and __getitem__."""
        # add
        self.assertTrue(len(self.tokens)==3)
        s=str(self.tok1)+'_NEW_Token_'+str(self.tok2)+'_NEW_Token_'+str(self.tok3)
        self.assertTrue(str(self.tokens)==s)
        tokens = self.tokens + self.tokens
        self.assertTrue(len(tokens)==6)
        self.assertTrue(str(tokens)==s+'_NEW_Token_'+s)
        tokens = tokens + self.tokens
        tokens += self.tokens
        self.assertTrue(len(tokens)==12)
        self.assertTrue(str(tokens)==(s+'_NEW_Token_')*3+s)
        # append
        tokens = self.tokens.copy()
        tokens.append(self.tokens[0])
        tokens.append(self.tokens[1])
        tokens.append(self.tokens[2])
        self.assertTrue(len(tokens)==6)
        self.assertTrue(str(tokens)==s+'_NEW_Token_'+s)
        # extend
        tokens.extend(self.tokens+self.tokens)
        self.assertTrue(len(tokens)==12)
        self.assertTrue(str(tokens)==(s+'_NEW_Token_')*3+s)
        tokens.insert(3,self.tokens[0])
        tokens.insert(4,self.tokens[1])
        tokens.insert(5,self.tokens[2])
        self.assertTrue(len(tokens)==15)
        self.assertTrue(str(tokens)==(s+'_NEW_Token_')*4+s)
        # getitem
        self.assertTrue(tokens[0] == self.tok1)
        self.assertTrue(tokens[1] == self.tok2)
        self.assertTrue(tokens[2] == self.tok3)
        bools = [tok==self.tok1 for tok in tokens[0::3]]
        self.assertTrue(all(bools))
        bools = [tok==self.tok2 for tok in tokens[1::3]]
        self.assertTrue(all(bools))
        bools = [tok==self.tok3 for tok in tokens[2::3]]
        self.assertTrue(all(bools))
        return None
    
    def test_attributesBehaviors(self,):
        self.tok1.setattr('test1',a=1,b=2)
        self.tok2.setattr('test2',c=3,d=4)
        self.tok2.setattr('test1',a=1,b=3)
        self.tok3.setattr('test3',e=5,f=6)
        self.tok3.setattr('test2',c=5,d=7)
        self.tok3.setattr('test1',a=2,b=2)
        tokens = Tokens([self.tok1,self.tok2,self.tok3])
        self.assertTrue(len(self.tokens)==3)
        self.assertTrue(tokens.attributes_keys == {'test1','test2','test3'})
        with self.assertRaises(AttributeError):
            tokens.attributes_keys = 4
        self.assertTrue(tokens.attributes_map == {'test1': [0, 1, 2], 'test3': [2], 'test2': [1, 2]})
        with self.assertRaises(AttributeError):
            tokens.attributes_map = 4
        self.assertTrue(tokens.attributes_map['test1'] == [0, 1, 2])
        self.assertTrue(tokens.attributes_map['test2'] == [1, 2])
        self.assertTrue(tokens.attributes_map['test3'] == [2])
        self.assertTrue(type(tokens.attributes_values)==dict)
        self.assertTrue(tokens.attributes_values['test1'] == [{'a': 1, 'b': 2}, {'a': 1, 'b': 3}, {'a': 2, 'b': 2}])
        self.assertTrue(tokens.attributes_values['test2'] == [{}, {'c': 3, 'd': 4}, {'c': 5, 'd': 7}])
        self.assertTrue(tokens.attributes_values['test3'] == [{}, {}, {'e': 5, 'f': 6}])
        tokens_test1 = tokens.has_attribute('test1')
        self.assertTrue(len(tokens_test1) == 3)
        tokens_test2 = tokens.has_attribute('test2')
        self.assertTrue(len(tokens_test2) == 2)
        tokens_test3 = tokens.has_attribute('test3')
        self.assertTrue(len(tokens_test3) == 1)
        self.assertTrue(tokens['test1'] == [{'a': 1, 'b': 2}, {'a': 1, 'b': 3}, {'a': 2, 'b': 2}])
        self.assertTrue(tokens['test2'] == [{}, {'c': 3, 'd': 4}, {'c': 5, 'd': 7}])
        self.assertTrue(tokens['test3'] == [{}, {}, {'e': 5, 'f': 6}])
        self.tok1.setattr('test_test',p=0,q=0)
        self.assertTrue(tokens.attributes_keys == {'test1','test2','test3'})
        tokens[0].setattr('test_test',p=1,q=1)
        self.assertTrue(tokens.attributes_keys == {'test1','test2','test3','test_test'})
        self.assertTrue(tokens.attributes_map == {'test1': [0, 1, 2], 'test_test': [0], 
                                  'test2': [1, 2], 'test3': [2]})
        return None

if __name__ == '__main__':
    ut.main()
